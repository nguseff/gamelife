﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLife
{
    public class GameEngine : IGameLifeEngine
    {
        GameState CurrentState;
        GameState PreviousState;
        int _livingCells;

        public int LivingCells
        {
            get
            {
                return _livingCells;
            }
        }

        public GameState State
        {
            get
            {
                return CurrentState;
            }
        }

        public GameEngine(int size)
        {
            CurrentState = new GameState(size);
            PreviousState = new GameState(size);
        }

        public void GenerateRandomState()
        {
            _livingCells = 0;
            var r = new Random();
            for (var i = 0; i < CurrentState.Size; i++)
            {
                for (var j = 0; j < CurrentState.Size; j++)
                {
                    CurrentState[i, j] = ((r.Next(100) % 100) > 50) ? GameState.Alive : GameState.Dead;
                    if (CurrentState[i, j] == GameState.Alive)
                        _livingCells++;
                }
            }
            CurrentState.CopyTo(PreviousState);
        }

        public bool GameTick()
        {
            CurrentState.CopyTo(PreviousState);
            int livingCells;
            for (var i = 0; i < PreviousState.Size; i++)
            {
                for (var j = 0; j < PreviousState.Size; j++)
                {
                    livingCells = CellRegion(i, j);
                    if((PreviousState[i, j] == GameState.Alive) && (livingCells < 2 || livingCells > 3))
                    {
                        CurrentState[i, j] = GameState.Dead;
                        _livingCells--;
                    }
                    if((PreviousState[i, j] == GameState.Dead) && livingCells == 3)
                    {
                        CurrentState[i, j] = GameState.Alive;
                        _livingCells++;
                    }
                }
            }
            return _livingCells > 0;
        }

        /// <summary>
        /// Количество живых клеток в окрестности
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private int CellRegion(int x, int y)
        {
            int livingCells = 0;
            if (PreviousState[x + 1, y])
                livingCells++;
            if (PreviousState[x, y + 1])
                livingCells++;
            if (PreviousState[x + 1, y + 1])
                livingCells++;
            if (PreviousState[x - 1, y])
                livingCells++;
            if (PreviousState[x, y - 1])
                livingCells++;
            if (PreviousState[x - 1, y - 1])
                livingCells++;
            if (PreviousState[x - 1, y + 1])
                livingCells++;
            if (PreviousState[x + 1, y - 1])
                livingCells++;
            return livingCells;
        }
    }
}
