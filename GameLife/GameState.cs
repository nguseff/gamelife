﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLife
{
    public class GameState
    {
        /// <summary>
        /// К полю добавляются поля в одну клетку (пустые), чтобы не нужно было постоянно проверять клетки на выход за границы массива
        /// </summary>
        public bool[,] GameField;
        public int Size { get; set; }

        public const bool Alive = true;
        public const bool Dead = false;

        public GameState(int size)
        {
            this.Size = size;
            this.GameField = new bool[size + 2, size + 2];
        }

        public void CopyTo(GameState state)
        {
            if (state.Size != this.Size)
                throw new ArgumentException();

            for (var i = 1; i < this.Size + 1; i++)
            {
                for (var j = 0; j < this.Size + 1; j++)
                {
                    state.GameField[i, j] = this.GameField[i, j];
                }
            }
        }

        public bool this[int i, int j]
        {
            get
            {
                return GameField[i + 1, j + 1];
            }
            set
            {
                GameField[i + 1, j + 1] = value;
            }
        }
    }
}
