﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLife
{
    class ConsoleGameRenderer
    {
        IGameLifeEngine Engine;

        public ConsoleGameRenderer(IGameLifeEngine engine)
        {
            this.Engine = engine;
        }


        public void render()
        {
            var size = Engine.State.Size;

            for (var i = 0; i < Engine.State.Size; i++)
            {
                for (var j = 0; j < Engine.State.Size; j++)
                {
                    Console.Write(Engine.State[i, j] == GameState.Alive ? "1" : "0");
                    Console.Write(" ");
                }
                Console.WriteLine();
            }

            Console.WriteLine();
        }

        public bool GameTick()
        {
            Engine.GameTick();
            render();
            return Engine.LivingCells > 0;
        }
        
    }
}
