﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLife
{
    interface IGameLifeEngine
    {
        void GenerateRandomState();
        bool GameTick();
        GameState State { get; }
        int LivingCells { get; }
    }
}
