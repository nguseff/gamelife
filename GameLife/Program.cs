﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLife
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите размер поля");
            int n = int.Parse(Console.ReadLine());

            var engine = new GameEngine(n);
            var renderer = new ConsoleGameRenderer(engine);

            engine.GenerateRandomState();
            Console.WriteLine("Начальное состояние");
            renderer.render();

            var moves = 0;
            while(engine.GameTick())
            {
                moves++;
                renderer.render();
                Console.WriteLine("Количество живых клеток - {0}", engine.LivingCells);
                Console.WriteLine("Нажмите Enter для следующего хода");
                Console.ReadLine();
            }

            Console.WriteLine("Игра закончена, количество ходов - {0}", moves);
            Console.ReadLine();
        }
    }
}
